<?php

namespace ServiceCore\Report\Collection;

use ArrayIterator;
use Countable;
use IteratorAggregate;

class Report implements Countable, IteratorAggregate
{
    /** @var array */
    private $reports;

    public function __construct(array $reports)
    {
        $this->reports = $reports;
    }

    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->reports);
    }

    public function count(): int
    {
        return \count($this->reports);
    }
}
