<?php

namespace ServiceCore\Report\Hydrator\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\Report\Hydrator\Report as ReportHydrator;

class Report implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): ReportHydrator
    {
        return new ReportHydrator($container->get(EntityManager::class), true);
    }
}
