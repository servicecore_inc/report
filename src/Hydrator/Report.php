<?php

namespace ServiceCore\Report\Hydrator;

use Doctrine\Common\Collections\ArrayCollection;
use DoctrineModule\Stdlib\Hydrator\DoctrineObject;
use InvalidArgumentException;
use ServiceCore\Report\Data\Report as ReportEntity;

class Report extends DoctrineObject
{
    public function extract($object): array
    {
        if (!$object instanceof ReportEntity) {
            throw new InvalidArgumentException(
                \sprintf(
                    '%s expects an instance of `%s`, given `%s`',
                    __CLASS__,
                    ReportEntity::class,
                    \is_object($object) ? \get_class($object) : \gettype($object)
                )
            );
        }

        return [
            'name'        => $object->getName(),
            'description' => $object->getDescription(),
            'results'     => $this->extractCollection($object->getResults()),
            'metadata'    => $object->getMetadata(),
            'page_count'  => $object->getPageCount(),
            'page_size'   => $object->getPageSize(),
            'total_items' => $object->getTotalItems(),
            'page'        => $object->getPage(),
        ];
    }

    private function extractCollection(ArrayCollection $collection): array
    {
        $results = [];

        foreach ($collection->getIterator() as $key => $item) {
            if ($item instanceof ArrayCollection) {
                $results[$key] = $this->extractCollection($item);
            } elseif (\is_object($item)) {
                $results[] = parent::extract($item);
            } else {
                $results[$key] = $item;
            }
        }

        return $results;
    }
}
