<?php

namespace ServiceCore\Report\Transformer;

use Doctrine\Common\Collections\ArrayCollection;

// A transformer (or data-pipe) is a second-step method after fetching the DTOs that might perform in-code
// translations. Either calculations which can't/shouldn't be done in SQL, or formatting changes to
// arrange data differently. A good example is the aging-receivables report, it's valid to break down the report
// by totals per customer, or a breakdown by customer of each invoice. The base provider should return
// a list of basic DTOs about invoices, and there should be two transformers that each mutate the data into
// the expected structure.
interface TransformerInterface
{
    // A transformer must be passed an array collection of DTOs and must return an array collection of DTOs
    // It can be modified or changed, or even completely replaced, but it must return a collection of DTOs
    // for the purposes of displaying things generically
    public function transform(ArrayCollection $data): ArrayCollection;
}
