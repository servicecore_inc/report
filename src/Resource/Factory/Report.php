<?php

namespace ServiceCore\Report\Resource\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\Report\Resource\Report as ReportResource;
use ServiceCore\Report\ServiceManager\ReportManager;

class Report implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): ReportResource
    {
        /** @var ReportManager $reportManager */
        $reportManager = $container->get(ReportManager::class);

        $resource = new ReportResource($reportManager);
        $em       = $container->get(EntityManager::class);

        $resource->setObjectManager($em);

        return $resource;
    }
}
