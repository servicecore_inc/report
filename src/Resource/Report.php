<?php

namespace ServiceCore\Report\Resource;

use Laminas\ApiTools\ContentNegotiation\Request as ContentNegotiationRequest;
use Laminas\ApiTools\Doctrine\Server\Resource\DoctrineResource;
use ServiceCore\Report\Collection\Report as ReportCollection;
use ServiceCore\Report\Data\Report as ReportEntity;
use ServiceCore\Report\Provider\MetadataProviderInterface;
use ServiceCore\Report\ServiceManager\ReportManager;

class Report extends DoctrineResource
{
    /** @var ReportManager */
    private $reports;

    public function __construct(ReportManager $reportManager)
    {
        parent::__construct();

        $this->reports = $reportManager;
    }

    public function fetchAll($data = []): ReportCollection
    {
        $reports = [];

        foreach ($this->reports->get('config') as $name => $config) {
            $reports[] = [
                'name'        => $name,
                'description' => $config['description'],
                'queryParams' => $config['queryParams'],
            ];
        }

        return new ReportCollection($reports);
    }

    public function fetch($id): ReportEntity
    {
        /** @var ContentNegotiationRequest $request */
        $request = $this->getEvent()->getRequest();

        /** @var ReportEntity $report */
        $report = $this->reports->get($id);
        $params = $this->getEvent()->getQueryParams();

        if ($params && \property_exists($params, 'perPage') && \property_exists($params, 'page')) {
            $report->setPage($params->page);
            $report->setPageSize($params->perPage);
        }

        $provider = $report->getProvider();

        // @todo clean up CSV rendering logic and decouple the report object from the CSV renderer
        if (($header = $request->getHeader('Accept')) && $header->getFieldValue() === 'text/csv') {
            $report->setResults($provider->retrieveUnpaginatedData($this->getEvent()));

            if ($transformer = $report->getTransformer()) {
                $report->setResults($transformer->transform($report->getResults()));
            }

            return $report;
        }

        $results = $provider->retrieveData($this->getEvent(), $report->getPage(), $report->getPageSize());

        if ($transformer = $report->getTransformer()) {
            $results = $transformer->transform($results);
        }

        $report->setResults($results);

        if ($provider instanceof MetadataProviderInterface) {
            $report->setMetadata($provider->getMetadata($this->getEvent()));
        }

        $itemCount = $provider->getTotalItemCount($this->getEvent());

        $report->setTotalItems($itemCount);
        $report->setPageCount(\ceil($itemCount / $report->getPageSize()));

        return $report;
    }
}
