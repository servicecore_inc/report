<?php

namespace ServiceCore\Report\ServiceManager\Factory;

use Interop\Container\ContainerInterface;
use InvalidArgumentException;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Laminas\ServiceManager\ServiceManager;
use ServiceCore\Report\ServiceManager\ReportManager as Manager;

class ReportManager implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): Manager
    {
        /** @var array $config */
        $config = $container->get('config');

        if (!\array_key_exists('reporting', $config) || !\array_key_exists('service_manager', $config['reporting'])) {
            throw new InvalidArgumentException(
                'The configuration array must define a `reporting` key, which itself must have a ' .
                '`service_manager` and `reports` key.');
        }

        $config['reporting']['service_manager']['services'][ServiceManager::class] = $container;

        return new Manager($container, $config['reporting']['reports'], $config['reporting']['service_manager']);
    }
}
