<?php

namespace ServiceCore\Report\ServiceManager;

use Laminas\ServiceManager\ServiceManager as BaseServiceManager;
use ServiceCore\Report\Factory\Report as ReportFactory;

class ReportManager extends BaseServiceManager
{
    /** @var BaseServiceManager */
    private $serviceManager;

    public function __construct(BaseServiceManager $serviceManager, array $reports, array $services = [])
    {
        $this->serviceManager = $serviceManager;

        // This will make the basic report config available in the manager, which is useful when fetching
        // the report objects or returning the descriptions
        $services['services']['config'] = $reports;

        // Next we want to alias every report name to the report factory
        foreach ($reports as $reportName => $report) {
            $services['factories'][$reportName] = ReportFactory::class;
        }

        parent::__construct($services);
    }

    public function get($name)
    {
        if ($this->has($name)) {
            return parent::get($name);
        }

        return $this->serviceManager->get($name);
    }
}
