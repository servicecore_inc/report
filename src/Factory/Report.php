<?php

namespace ServiceCore\Report\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use ServiceCore\Report\Data\Report as ReportEntity;
use ServiceCore\Report\Provider\AbstractProvider;

class Report implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): ReportEntity
    {
        $config     = $container->get('config');
        $reportData = $config[$requestedName];
        $provider   = $container->get($reportData['provider']);

        if ($provider instanceof AbstractProvider) {
            $provider->setFilterManager($container->get('ZfDoctrineQueryBuilderFilterManagerOrm'));
            $provider->setOrderByManager($container->get('ZfDoctrineQueryBuilderOrderByManagerOrm'));
        }

        $transformer = null;
        if (\array_key_exists('transformer', $reportData) && $container->has($reportData['transformer'])) {
            $transformer = $container->get($reportData['transformer']);
        }

        $report = new ReportEntity($provider, $transformer);

        $report->setName($requestedName);
        $report->setDescription($reportData['description']);

        return $report;
    }
}
