<?php

namespace ServiceCore\Report\Data;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use ServiceCore\Report\Provider\AbstractProvider;
use ServiceCore\Report\Transformer\TransformerInterface;

/**
 * @ORM\Entity(readOnly=true)
 */
class Report
{
    /**
     * @var string
     * @ORM\Column(name="name", type="string")
     * @ORM\Id()
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="description", type="string")
     */
    private $description;

    /**
     * @var ArrayCollection
     * @ORM\Column(name="results")
     */
    private $results;

    /**
     * @var array
     * @ORM\Column(name="metadata")
     */
    private $metadata = [];

    /**
     * @var AbstractProvider
     */
    private $provider;

    /**
     * @var TransformerInterface|null
     */
    private $transformer;

    /** @var int  */
    private $pageCount = 1;

    /** @var int  */
    private $pageSize = 10;

    /** @var int */
    private $totalItems = 0;

    /** @var int */
    private $page = 1;

    public function __construct(AbstractProvider $provider, ?TransformerInterface $transformer = null)
    {
        $this->provider    = $provider;
        $this->transformer = $transformer;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(?string $name = null): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description = null): self
    {
        $this->description = $description;

        return $this;
    }

    public function getProvider(): AbstractProvider
    {
        return $this->provider;
    }

    public function setProvider(AbstractProvider $provider): self
    {
        $this->provider = $provider;

        return $this;
    }

    public function getTransformer(): ?TransformerInterface
    {
        return $this->transformer;
    }

    public function setTransformer(?TransformerInterface $transformer = null)
    {
        $this->transformer = $transformer;

        return $this;
    }

    public function getResults(): ArrayCollection
    {
        return $this->results;
    }

    public function setResults(ArrayCollection $results): self
    {
        $this->results = $results;

        return $this;
    }

    public function getMetadata(): array
    {
        return $this->metadata;
    }

    public function setMetadata(array $metadata): self
    {
        $this->metadata = $metadata;

        return $this;
    }

    public function getPageCount(): int
    {
        return $this->pageCount;
    }

    public function setPageCount(int $pageCount): self
    {
        $this->pageCount = $pageCount;

        return $this;
    }

    public function getPageSize(): int
    {
        return $this->pageSize;
    }

    public function setPageSize(int $pageSize): self
    {
        $this->pageSize = $pageSize;

        return $this;
    }

    public function getTotalItems(): int
    {
        return $this->totalItems;
    }

    public function setTotalItems(int $totalItems): self
    {
        $this->totalItems = $totalItems;

        return $this;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function setPage(int $page): self
    {
        $this->page = $page;

        return $this;
    }
}
