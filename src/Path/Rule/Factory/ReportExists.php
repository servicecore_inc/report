<?php

namespace ServiceCore\Report\Path\Rule\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use RuntimeException;
use ServiceCore\Path\Rule\RuleInterface;
use ServiceCore\Report\Path\Rule\ReportExists as ReportExistsRule;

class ReportExists implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, ?array $options = null): RuleInterface
    {
        $config = $container->get('config');

        if (!\array_key_exists('reporting', $config) || !\array_key_exists('reports', $config['reporting'])) {
            throw new RuntimeException('Config array must define `reporting` and `reports` key.');
        }

        /** @var EntityManager $entityManager */
        $entityManager = $container->get(EntityManager::class);
        $reportNames   = \array_keys($config['reporting']['reports']);

        return new ReportExistsRule($entityManager, ['reportNames' => $reportNames]);
    }
}
