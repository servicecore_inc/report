<?php

namespace ServiceCore\Report\Path\Rule;

use ServiceCore\Path\Rule\Rule;

class ReportExists extends Rule
{
    public function apply(array $parameters): bool
    {
        return \array_key_exists('report_id', $parameters) &&
            \in_array($parameters['report_id'], $this->options['reportNames'], true);
    }
}
