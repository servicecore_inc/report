<?php

namespace ServiceCore\Report\Provider;

use Laminas\EventManager\EventInterface;

interface MetadataProviderInterface
{
    public function getMetadata(EventInterface $event): array;
}
