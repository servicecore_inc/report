<?php

namespace ServiceCore\Report\Provider;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Laminas\ApiTools\ContentNegotiation\Request;
use Laminas\ApiTools\Doctrine\QueryBuilder\Filter\Service\ORMFilterManager;
use Laminas\ApiTools\Doctrine\QueryBuilder\OrderBy\Service\ORMOrderByManager;
use Laminas\ApiTools\Doctrine\Server\Paginator\Adapter\DoctrineOrmAdapter;
use Laminas\ApiTools\Rest\ResourceEvent;
use Laminas\EventManager\EventInterface;

abstract class AbstractProvider implements ObjectManagerAwareInterface
{
    use ProvidesObjectManager;

    /** @var QueryBuilder */
    protected $queryBuilder;

    /** @var ORMFilterManager */
    private $filterManager;

    /** @var ORMOrderByManager */
    private $orderByManager;

    abstract public function getTotalItemCount(EventInterface $event): int;

    abstract protected function buildQueryBuilder(EventInterface $event): void;

    public function getFilterManager(): ORMFilterManager
    {
        return $this->filterManager;
    }

    public function setFilterManager(ORMFilterManager $filterManager): self
    {
        $this->filterManager = $filterManager;

        return $this;
    }

    public function getOrderByManager(): ORMOrderByManager
    {
        return $this->orderByManager;
    }

    public function setOrderByManager(ORMOrderByManager $orderByManager): self
    {
        $this->orderByManager = $orderByManager;

        return $this;
    }

    public function retrieveData(ResourceEvent $event, int $page = 1, int $pageSize = 10): ArrayCollection
    {
        return new ArrayCollection($this->createQuery($event)->getItems(($page - 1) * $pageSize, $pageSize));
    }

    public function retrieveUnpaginatedData(ResourceEvent $event): ArrayCollection
    {
        $query = $this->createQuery($event);

        return new ArrayCollection($query->getItems(0, $query->count()));
    }

    protected function createQuery(ResourceEvent $event): DoctrineOrmAdapter
    {
        $this->queryBuilder = $this->getObjectManager()->createQueryBuilder();

        $this->buildQueryBuilder($event);
        $this->applyFilter($event);
        $this->applyOrderBy($event);

        return new DoctrineOrmAdapter($this->queryBuilder->getQuery());
    }

    private function applyFilter(EventInterface $event): void
    {
        /**
         * @var ResourceEvent $event
         * @var Request $request
         */
        $request = $event->getRequest();

        if ($request === null) {
            return;
        }

        $params = $request->getQuery();

        if ($params->offsetExists('filter')) {
            $this->getFilterManager()->filter($this->queryBuilder, null, $params->offsetGet('filter'));
        }
    }

    private function applyOrderBy(EventInterface $event): void
    {
        /**
         * @var ResourceEvent $event
         * @var Request $request
         */
        $request = $event->getRequest();

        if ($request === null) {
            return;
        }

        $params = $request->getQuery();

        if ($params->offsetExists('order-by')) {
            $this->getOrderByManager()->orderBy($this->queryBuilder, null, $params->offsetGet('order-by'));
        }
    }
}
