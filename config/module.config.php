<?php

namespace ServiceCore\Report;

use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use ServiceCore\Report\Hydrator\Factory\Report as ReportHydratorFactory;
use ServiceCore\Report\Hydrator\Report as ReportHydrator;
use ServiceCore\Report\Path\Rule\Factory\ReportExists as ReportExistsFactory;
use ServiceCore\Report\Path\Rule\ReportExists;
use ServiceCore\Report\Resource\Factory\Report as ReportFactory;
use ServiceCore\Report\Resource\Report as ReportResource;
use ServiceCore\Report\ServiceManager\Factory\ReportManager as ReportManagerFactory;
use ServiceCore\Report\ServiceManager\ReportManager;

return [
    'doctrine'        => [
        'driver' => [
            'orm_default'   => [
                'drivers' => [
                    __NAMESPACE__ => 'report_driver',
                ],
            ],
            'report_driver' => [
                'class' => AnnotationDriver::class,
                'paths' => [
                    \dirname(__DIR__) . '/src/Data',
                ],
            ],
        ],
    ],
    'service_manager' => [
        'factories' => [
            ReportManager::class  => ReportManagerFactory::class,
            ReportResource::class => ReportFactory::class,
            ReportHydrator::class => ReportHydratorFactory::class,
            ReportExists::class   => ReportExistsFactory::class,
        ],
    ],
];
