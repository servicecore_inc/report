<?php

namespace ServiceCore\Report\Test\Path\Rule;

use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;
use ServiceCore\Report\Path\Rule\ReportExists;
use stdClass;

class ReportExistsTest extends TestCase
{
    public function testApplyReturnsFalse(): void
    {
        $emBuilder = $this->getMockBuilder(EntityManager::class);

        $emBuilder->disableOriginalConstructor();

        $em      = $emBuilder->getMock();
        $options = [
            'reportNames' => [
                stdClass::class
            ]
        ];

        $rule = new ReportExists($em, $options);

        $this->assertFalse($rule->apply(['report_id' => 'fooBar']));
    }

    public function testApplyReturnsTrue(): void
    {
        $emBuilder = $this->getMockBuilder(EntityManager::class);

        $emBuilder->disableOriginalConstructor();

        $em      = $emBuilder->getMock();
        $options = [
            'reportNames' => [
                stdClass::class
            ]
        ];

        $rule = new ReportExists($em, $options);

        $this->assertTrue($rule->apply(['report_id' => stdClass::class]));
    }
}
