<?php

namespace ServiceCore\Report\Test\Path\Rule\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use PHPUnit\Framework\TestCase;
use RuntimeException;
use ServiceCore\Report\Path\Rule\Factory\ReportExists as ReportExistsFactory;
use ServiceCore\Report\Path\Rule\ReportExists;

class ReportExistsTest extends TestCase
{
    public function testInvokeThrowsRuntimeExceptionIfKeysMissing(): void
    {
        $factory          = new ReportExistsFactory();
        $containerBuilder = $this->getMockBuilder(ContainerInterface::class);

        $containerBuilder->onlyMethods(
            [
                'get',
                'has'
            ]
        );

        $container = $containerBuilder->getMock();
        $config    = [
            'reporting' => []
        ];

        $container->expects($this->once())
            ->method('get')
            ->willReturn($config);

        $this->expectException(RuntimeException::class);

        $factory($container, ReportExists::class);
    }

    public function testInvoke(): void
    {
        $factory          = new ReportExistsFactory();
        $containerBuilder = $this->getMockBuilder(ContainerInterface::class);

        $containerBuilder->onlyMethods(
            [
                'get',
                'has'
            ]
        );

        $name1     = 'fooBar1';
        $name2     = 'fooBar2';
        $container = $containerBuilder->getMock();
        $config    = [
            'reporting' => [
                'reports' => [
                    $name1 => [],
                    $name2 => []
                ]
            ]
        ];

        $emBuilder = $this->getMockBuilder(EntityManager::class);

        $emBuilder->disableOriginalConstructor();

        $em = $emBuilder->getMock();

        $container->expects($this->atLeastOnce())
            ->method('get')
            ->willReturnOnConsecutiveCalls($config, $em);

        $factory($container, ReportExists::class);
    }
}
