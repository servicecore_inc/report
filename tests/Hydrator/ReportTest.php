<?php

namespace ServiceCore\Report\Test\Hydrator;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use ServiceCore\Report\Data\Report;
use ServiceCore\Report\Hydrator\Report as ReportHydrator;
use ServiceCore\Report\Test\Support\Mock\Provider as MockProvider;
use stdClass;

class ReportTest extends TestCase
{
    public function testExtractThrowsExceptionIfObjectNotCorrectInstance(): void
    {
        $objectManagerBuilder = $this->getMockBuilder(EntityManager::class);

        $objectManagerBuilder->disableOriginalConstructor();

        $objectManager = $objectManagerBuilder->getMock();
        $hydrator      = new ReportHydrator($objectManager);
        $object        = new stdClass();

        $this->expectException(InvalidArgumentException::class);

        $hydrator->extract($object);
    }

    public function testExtract(): void
    {
        $objectManagerBuilder = $this->getMockBuilder(EntityManager::class);

        $objectManagerBuilder->disableOriginalConstructor();

        $objectManager = $objectManagerBuilder->getMock();
        $hydrator      = new ReportHydrator($objectManager);
        $object        = new Report(new MockProvider());

        $name        = 'fooBar';
        $description = 'fooBarDesc';
        $metadata    = ['fooBarMeta'];
        $results     = new ArrayCollection(['fooBar']);
        $pageCount   = 10;
        $pageSize    = 100;
        $totalItems  = 1100;
        $page        = 1;

        $object->setName($name)
            ->setDescription($description)
            ->setResults($results)
            ->setMetadata($metadata)
            ->setPageCount($pageCount)
            ->setPageSize($pageSize)
            ->setTotalItems($totalItems)
            ->setPage($page);

        $data = $hydrator->extract($object);

        $this->assertArrayHasKey('name', $data);
        $this->assertEquals($name, $data['name']);

        $this->assertArrayHasKey('description', $data);
        $this->assertEquals($description, $data['description']);

        $this->assertArrayHasKey('results', $data);
        $this->assertIsArray($data['results']);
        $this->assertNotEmpty($data['results']);

        $this->assertArrayHasKey('metadata', $data);
        $this->assertEquals($metadata, $data['metadata']);

        $this->assertArrayHasKey('page_count', $data);
        $this->assertEquals($pageCount, $data['page_count']);

        $this->assertArrayHasKey('page_size', $data);
        $this->assertEquals($pageSize, $data['page_size']);

        $this->assertArrayHasKey('page', $data);
        $this->assertEquals($page, $data['page']);

        $this->assertArrayHasKey('total_items', $data);
        $this->assertEquals($totalItems, $data['total_items']);
    }
}
