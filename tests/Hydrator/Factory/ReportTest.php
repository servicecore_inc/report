<?php

namespace ServiceCore\Report\Test\Hydrator\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use PHPUnit\Framework\TestCase;
use ServiceCore\Report\Hydrator\Factory\Report as ReportHydratorFactory;
use ServiceCore\Report\Hydrator\Report as ReportHydrator;

class ReportTest extends TestCase
{
    public function testInvoke(): void
    {
        $factory          = new ReportHydratorFactory();
        $containerBuilder = $this->getMockBuilder(ContainerInterface::class);

        $containerBuilder->onlyMethods(
            [
                'get',
                'has'
            ]
        );

        $container = $containerBuilder->getMock();
        $emBuilder = $this->getMockBuilder(EntityManager::class);

        $emBuilder->disableOriginalConstructor();

        $em = $emBuilder->getMock();

        $container->expects($this->once())
            ->method('get')
            ->willReturn($em);

        $factory($container, ReportHydrator::class);
    }
}
