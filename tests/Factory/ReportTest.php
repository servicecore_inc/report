<?php

namespace ServiceCore\Report\Test\Factory;

use Interop\Container\ContainerInterface;
use Laminas\ApiTools\Doctrine\QueryBuilder\Filter\Service\ORMFilterManager;
use Laminas\ApiTools\Doctrine\QueryBuilder\OrderBy\Service\ORMOrderByManager;
use PHPUnit\Framework\TestCase;
use ServiceCore\Report\Factory\Report as ReportFactory;
use ServiceCore\Report\Test\Support\Mock\Provider as MockProvider;
use ServiceCore\Report\Test\Support\Mock\Transformer as MockTransformer;
use stdClass;

class ReportTest extends TestCase
{
    public function testInvoke(): void
    {
        $factory          = new ReportFactory();
        $containerBuilder = $this->getMockBuilder(ContainerInterface::class);

        $containerBuilder->onlyMethods(
            [
                'get',
                'has'
            ]
        );

        $description = 'fooBar';
        $container   = $containerBuilder->getMock();
        $firstGet    = [
            stdClass::class => [
                'provider'    => stdClass::class,
                'description' => $description,
                'transformer' => stdClass::class
            ]
        ];

        $secondGet       = new MockProvider();
        $thirdGetBuilder = $this->getMockBuilder(ORMFilterManager::class);

        $thirdGetBuilder->disableOriginalConstructor();

        $thirdGet         = $thirdGetBuilder->getMock();
        $fourthGetBuilder = $this->getMockBuilder(ORMOrderByManager::class);

        $fourthGetBuilder->disableOriginalConstructor();

        $fourthGet = $fourthGetBuilder->getMock();
        $fifthGet  = new MockTransformer();

        $container->expects($this->once())->method('has')->willReturn(true);

        $container->expects($this->atLeastOnce())
            ->method('get')
            ->willReturnOnConsecutiveCalls($firstGet, $secondGet, $thirdGet, $fourthGet, $fifthGet);

        $report = $factory($container, stdClass::class);

        $this->assertSame($secondGet, $report->getProvider());
        $this->assertSame($fifthGet, $report->getTransformer());
        $this->assertEquals(stdClass::class, $report->getName());
        $this->assertEquals($description, $report->getDescription());
    }
}
