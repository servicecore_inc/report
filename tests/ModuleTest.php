<?php

namespace ServiceCore\Report\Test;

use PHPUnit\Framework\TestCase;
use ServiceCore\Report\Module;

class ModuleTest extends TestCase
{
    public function testGetConfig(): void
    {
        $module = new Module();

        $this->assertNotEmpty($module->getConfig());
    }
}
