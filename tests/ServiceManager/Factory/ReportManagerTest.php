<?php

namespace ServiceCore\Report\Test\ServiceManager\Factory;

use Interop\Container\ContainerInterface;
use InvalidArgumentException;
use Laminas\ServiceManager\ServiceManager;
use PHPUnit\Framework\TestCase;
use ServiceCore\Report\ServiceManager\Factory\ReportManager as ReportManagerFactory;
use ServiceCore\Report\ServiceManager\ReportManager;

class ReportManagerTest extends TestCase
{
    public function testInvokeThrowsInvalidArgumentException(): void
    {
        $factory          = new ReportManagerFactory();
        $containerBuilder = $this->getMockBuilder(ContainerInterface::class);

        $containerBuilder->onlyMethods(
            [
                'get',
                'has'
            ]
        );

        $container = $containerBuilder->getMock();
        $config    = [
            'reporting' => []
        ];

        $container->expects($this->once())
            ->method('get')
            ->willReturn($config);

        $this->expectException(InvalidArgumentException::class);

        $factory($container, ReportManager::class);
    }

    public function testInvoke(): void
    {
        $factory          = new ReportManagerFactory();
        $containerBuilder = $this->getMockBuilder(ServiceManager::class);

        $containerBuilder->onlyMethods(
            [
                'get',
            ]
        );

        $container = $containerBuilder->getMock();
        $config    = [
            'reporting' => [
                'reports'         => [],
                'service_manager' => []
            ]
        ];

        $container->expects($this->once())
            ->method('get')
            ->willReturn($config);

        $factory($container, ReportManager::class);
    }
}
