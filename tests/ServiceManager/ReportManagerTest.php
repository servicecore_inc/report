<?php

namespace ServiceCore\Report\Test\ServiceManager;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\ServiceManager;
use PHPUnit\Framework\TestCase;
use ServiceCore\Report\ServiceManager\ReportManager;
use stdClass;

class ReportManagerTest extends TestCase
{
    public function testGetWithClassFromBaseSM(): void
    {
        $containerBuilder = $this->getMockBuilder(ServiceManager::class);
        $container        = $containerBuilder->getMock();

        $containerBuilder->onlyMethods(
            [
                'get'
            ]
        );

        $container->expects($this->never())
            ->method('get');

        $services = [
            'services' => [
                'fooBar' => new stdClass()
            ]
        ];

        $serviceManager = new ReportManager($container, [], $services);

        $serviceManager->get('fooBar');
    }

    public function testGet(): void
    {
        $containerBuilder = $this->getMockBuilder(ServiceManager::class);
        $container        = $containerBuilder->getMock();
        $return           = new stdClass();

        $containerBuilder->onlyMethods(
            [
                'get'
            ]
        );
        $container->expects($this->once())
            ->method('get')
            ->willReturn($return);

        $serviceManager = new ReportManager($container, []);

        $this->assertSame($return, $serviceManager->get('fooBar'));
    }
}
