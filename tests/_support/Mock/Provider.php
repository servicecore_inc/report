<?php

namespace ServiceCore\Report\Test\Support\Mock;

use Laminas\EventManager\EventInterface;
use ServiceCore\Report\Provider\AbstractProvider;

class Provider extends AbstractProvider
{
    public function getTotalItemCount(EventInterface $event): int
    {
        return 1;
    }

    protected function buildQueryBuilder(EventInterface $event): void
    {
        return;
    }
}
