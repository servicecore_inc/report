<?php

namespace ServiceCore\Report\Test\Support\Mock;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManagerInterface;

class Query extends AbstractQuery
{
    public function __construct(EntityManagerInterface $em)
    {
        //nothing
    }

    public function setFirstResult(): void
    {

    }

    public function setMaxResults(): void
    {

    }

    public function getResult($hydrationMode = self::HYDRATE_OBJECT): array
    {
        return [];
    }

    public function getSQL()
    {
        // TODO: Implement getSQL() method.
    }

    protected function _doExecute()
    {
        // TODO: Implement _doExecute() method.
    }
}
