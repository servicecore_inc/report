<?php

namespace ServiceCore\Report\Test\Support\Mock;

use Doctrine\Common\Collections\ArrayCollection;
use ServiceCore\Report\Transformer\TransformerInterface;

class Transformer implements TransformerInterface
{
    /** @var array */
    private $results;

    public function __construct(array $results = [])
    {
        $this->results = $results;
    }

    public function transform(ArrayCollection $data): ArrayCollection
    {
        return new ArrayCollection($this->results);
    }
}
