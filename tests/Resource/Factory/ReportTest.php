<?php

namespace ServiceCore\Report\Test\Resource\Factory;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use PHPUnit\Framework\TestCase;
use ServiceCore\Report\Resource\Factory\Report as ReportResourceFactory;
use ServiceCore\Report\Resource\Report;
use ServiceCore\Report\ServiceManager\ReportManager;

class ReportTest extends TestCase
{
    public function testInvoke(): void
    {
        $factory          = new ReportResourceFactory();
        $containerBuilder = $this->getMockBuilder(ContainerInterface::class);

        $containerBuilder->onlyMethods(
            [
                'get',
                'has'
            ]
        );

        $container            = $containerBuilder->getMock();
        $reportManagerBuilder = $this->getMockBuilder(ReportManager::class);

        $reportManagerBuilder->disableOriginalConstructor();

        $reportManager = $reportManagerBuilder->getMock();
        $emBuilder     = $this->getMockBuilder(EntityManager::class);

        $emBuilder->disableOriginalConstructor();

        $em = $emBuilder->getMock();

        $container->expects($this->atLeastOnce())
            ->method('get')
            ->willReturnOnConsecutiveCalls($reportManager, $em);

        $factory($container, Report::class);
    }
}
