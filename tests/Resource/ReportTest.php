<?php

namespace ServiceCore\Report\Test\Resource;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Laminas\ApiTools\ContentNegotiation\Request as ContentNegotiationRequest;
use Laminas\ApiTools\Rest\ResourceEvent;
use Laminas\Stdlib\Parameters;
use PHPUnit\Framework\TestCase;
use ReflectionProperty;
use ServiceCore\Report\Data\Report as ReportEntity;
use ServiceCore\Report\Resource\Report;
use ServiceCore\Report\ServiceManager\ReportManager;
use ServiceCore\Report\Test\Support\Mock\Provider as MockProvider;
use ServiceCore\Report\Test\Support\Mock\Query as MockQuery;
use ServiceCore\Report\Test\Support\Mock\Transformer as MockTransformer;

class ReportTest extends TestCase
{
    public function testFetchAll(): void
    {
        $reportManagerBuilder = $this->getMockBuilder(ReportManager::class);

        $reportManagerBuilder->disableOriginalConstructor();

        $reportManagerBuilder->onlyMethods(
            [
                'get'
            ]
        );

        $reportManager = $reportManagerBuilder->getMock();
        $name          = 'fooName';
        $config        = [
            'description' => 'description!',
            'queryParams' => []
        ];

        $reports = [
            $name => $config
        ];

        $reportManager->expects($this->once())
            ->method('get')
            ->willReturn($reports);

        $report = new Report($reportManager);
        $result = $report->fetchAll([]);

        $this->assertNotEmpty($result);
    }

    public function testFetch(): void
    {
        $reportManagerBuilder = $this->getMockBuilder(ReportManager::class);

        $reportManagerBuilder->disableOriginalConstructor();

        $reportManagerBuilder->onlyMethods(
            [
                'get'
            ]
        );

        $reportManager = $reportManagerBuilder->getMock();
        $provider      = new MockProvider();
        $emBuilder     = $this->getMockBuilder(EntityManager::class);

        $emBuilder->disableOriginalConstructor();

        $emBuilder->onlyMethods(
            [
                'createQueryBuilder'
            ]
        );

        $em                  = $emBuilder->getMock();
        $queryBuilderBuilder = $this->getMockBuilder(QueryBuilder::class);

        $queryBuilderBuilder->onlyMethods(
            [
                'getQuery'
            ]
        );

        $queryBuilderBuilder->disableOriginalConstructor();

        $queryBuilder = $queryBuilderBuilder->getMock();

        $queryBuilder->expects($this->once())
            ->method('getQuery')
            ->willReturn(new MockQuery($em));

        $em->expects($this->once())
            ->method('createQueryBuilder')
            ->willReturn($queryBuilder);

        $provider->setObjectManager($em);

        $reportEntity = new ReportEntity($provider, new MockTransformer([1, 2]));

        $reportManager->expects($this->once())
            ->method('get')
            ->willReturn($reportEntity);

        $resourceEvent  = new ResourceEvent();
        $requestBuilder = $this->getMockBuilder(ContentNegotiationRequest::class);

        $requestBuilder->disableOriginalConstructor();

        $requestBuilder->onlyMethods(
            [
                'getQuery'
            ]
        );

        $request = $requestBuilder->getMock();

        $request->expects($this->atLeastOnce())
            ->method('getQuery')
            ->willReturn(new Parameters());

        $resourceEvent->setRequest($request);

        $report = new Report($reportManager);
        $prop   = new ReflectionProperty($report, 'event');

        $prop->setAccessible(true);
        $prop->setValue($report, $resourceEvent);

        $report->fetch(1);
        $this->assertNotEmpty($reportEntity->getResults());
    }
}
